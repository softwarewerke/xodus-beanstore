# xodus-beanstore   
***softwarewerke (c) 2016***

xodus-beanstore is a Java library that provides Java beans persistents on top of Xodus. 
[Xodus](https://github.com/JetBrains/xodus) is a *Java transactional schema-less embedded database* 
by JetBrains.

This library plugs in a bean mapper between key/value entities and Java beans to persist/retrieve
Java objects. Additional helper methods for object storing and retrieving are provided. 

Only primitive or String type values are supported. 

To avoid to persist a member use the `@java.beans.Transient` annotation on the getter.

The library persist recursively the complete object tree for all embedded objects that implements the 
`Identifiable` interface. That works for single objects (one to one), arrays and collections (one to many).

This can be controlled by the `@Transient` annotation and further to avoid 
data manipulation in the embedded objects by the `@com.softwarewerke.xodus.beanstore.NoCascade` annotation on the getter.
`@NoCascade` will load the data, but not writing nor removing the embedded object(s).

You can find an example in `TestCustomer.java`.
 
The `iter()` methods returns a `BeanInterable` with an open transaction associated. 
The transaction ensures you will see the data as a snapshot. The iterable MUST be 
closed when done. The utility methods (`any()`,`list()`,...) by default are closing
the associated transaction. You may use the method with boolean `close` parameter to control
this. In this case you must call `BeanInterable#close()` or a non-arg utility method 
further in your code.
     
## Requirements

Java >=1.8

## Example
    
A first simple example. You can find more usage examples in the test package.

    // Open the store on disk. If not exists it will be created in this folder 
    PersistentBeanStore store = new PersistentBeanStore("./db");

    // Create and store a bean
    User user = new User();
    user.setName("joe");
    user.setAge(36);
    store.store(user);

    // Retrieve again by id
    user = (User) store.get(User.class,user.getId());

    // Retrieve by query fields
    user = (User) store.iter(User.class,"name","joe").any(); 

    // Retrive all matches into a list
    List<User> list = (User) store.iter(User.class,new RangeClause("age",30,45)).list();

    // Iterate lazily thru query result
    BeanIterable<User> iter = store.iter(User.class,new StartClause("name","j"));
    for(User uu : iter) {
       ...
    }

    // Close the iterable when done
    iter.close();

    // Close store when done
    store.close();

