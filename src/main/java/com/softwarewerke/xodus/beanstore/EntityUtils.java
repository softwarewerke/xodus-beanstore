/*
Copyright 2016 softwarewerke.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.softwarewerke.xodus.beanstore;

import java.beans.PropertyDescriptor;
import java.beans.Transient;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import jetbrains.exodus.entitystore.Entity;
import org.apache.commons.beanutils.PropertyUtils;

/**
 *
 * @author peter
 */
public final class EntityUtils {

    private EntityUtils() {
    }

    /**
     * Sets properties in the entity from the given Java bean, excludes "id",
     * "class" and @Transient annotated bean properties.
     *
     * @param obj
     * @param e
     */
    public static void intoEntity(Object obj, Entity e) {
        try {
            for (PropertyDescriptor d : PropertyUtils.getPropertyDescriptors(obj.getClass())) {
                if (!d.getName().equals("id") && !d.getName().equals("class")) {
                    Method meth = d.getReadMethod();
                    if (meth.getAnnotation(Transient.class) != null) {
                        continue;
                    }
                    Object val = meth.invoke(obj);
                    if (val instanceof Comparable) {
                        e.setProperty(d.getName(), (Comparable) val);
                    }
                }
            }
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        } catch (InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static <T> T toBean(Entity e, Class clazz) {
        T bean = null;
        try {
            bean = (T) clazz.newInstance();
            PropertyUtils.setSimpleProperty(bean, "id", e.getId().getLocalId());
            for (String n : e.getPropertyNames()) {
                PropertyUtils.setSimpleProperty(bean, n, e.getProperty(n));
            }
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        } catch (InvocationTargetException | NoSuchMethodException ex) {
            throw new RuntimeException(ex);
        }
        return bean;
    }
}
