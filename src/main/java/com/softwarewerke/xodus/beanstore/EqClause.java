/*
Copyright 2016 softwarewerke.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.softwarewerke.xodus.beanstore;

import java.util.Objects;

/**
 * A name/value pair used as equal clause in store querying.
 *
 * @author peter
 */
public class EqClause implements Clause {

    private final Oper oper;
    private final String fieldName;
    private final Comparable value;

    public EqClause(String fieldName, Comparable value) {
        this.oper = Oper.intersect;
        this.fieldName = fieldName;
        this.value = value;
    }

    public EqClause(Oper oper, String fieldName, Comparable value) {
        this.oper = oper;
        this.fieldName = fieldName;
        this.value = value;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Comparable getValue() {
        return value;
    }

    public Oper getOper() {
        return oper;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.oper);
        hash = 23 * hash + Objects.hashCode(this.fieldName);
        hash = 23 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EqClause other = (EqClause) obj;
        if (!Objects.equals(this.fieldName, other.fieldName)) {
            return false;
        }
        if (this.oper != other.oper) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EqClause{" + "oper=" + oper + ", fieldName=" + fieldName + ", value=" + value + '}';
    }

}
