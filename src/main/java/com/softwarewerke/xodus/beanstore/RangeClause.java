/*
Copyright 2016 softwarewerke.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.softwarewerke.xodus.beanstore;

import java.util.Objects;

/**
 * A name/value/value triple used as range clause in store querying.
 *
 * @author peter
 */
public class RangeClause implements Clause {

    private final Oper oper;
    private final String fieldName;
    private final Comparable from;
    private final Comparable to;

    public RangeClause(Oper oper, String fieldName, Comparable from, Comparable to) {
        this.oper = oper;
        this.fieldName = fieldName;
        // sanitisize from/to values
        if (from.compareTo(to) > 0) {
            Comparable x = from;
            from = to;
            to = x;
        }
        this.from = from;
        this.to = to;
    }

    public RangeClause(String fieldName, Comparable from, Comparable to) {
        this.oper = Oper.intersect;
        this.fieldName = fieldName;
        // sanitisize from/to values
        if (from.compareTo(to) > 0) {
            Comparable x = from;
            from = to;
            to = x;
        }
        this.from = from;
        this.to = to;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Comparable getFrom() {
        return from;
    }

    public Comparable getTo() {
        return to;
    }

    public Oper getOper() {
        return oper;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.oper);
        hash = 97 * hash + Objects.hashCode(this.fieldName);
        hash = 97 * hash + Objects.hashCode(this.from);
        hash = 97 * hash + Objects.hashCode(this.to);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RangeClause other = (RangeClause) obj;
        if (!Objects.equals(this.fieldName, other.fieldName)) {
            return false;
        }
        if (this.oper != other.oper) {
            return false;
        }
        if (!Objects.equals(this.from, other.from)) {
            return false;
        }
        if (!Objects.equals(this.to, other.to)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RangeClause{" + "oper=" + oper + ", fieldName=" + fieldName + ", from=" + from + ", to=" + to + '}';
    }

}
