/*
Copyright 2016 softwarewerke.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.softwarewerke.xodus.beanstore;

import com.softwarewerke.xodus.beanstore.Clause.Oper;
import static com.softwarewerke.xodus.beanstore.EntityUtils.intoEntity;
import static com.softwarewerke.xodus.beanstore.EntityUtils.toBean;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import jetbrains.exodus.entitystore.Entity;
import jetbrains.exodus.entitystore.EntityIterable;
import jetbrains.exodus.entitystore.EntityRemovedInDatabaseException;
import jetbrains.exodus.entitystore.PersistentEntityStore;
import jetbrains.exodus.entitystore.PersistentEntityStores;
import jetbrains.exodus.entitystore.StoreTransaction;
import org.jetbrains.annotations.NotNull;

/**
 * A persistent Java bean store, backed up by an PersistentEntityStore.
 *
 * @author peter
 */
public class PersistentBeanStore {

    PersistentEntityStore store;

    /**
     * Opens/Creates the store at the given file path, path MUST be a folder
     * name.
     *
     * @param path
     */
    public PersistentBeanStore(String path) {
        store = PersistentEntityStores.newInstance(path);
    }

    /**
     * Opens/Creates the store with the given file, file MUST point to an
     * folder.
     *
     * @param path
     */
    public PersistentBeanStore(File file) {
        store = PersistentEntityStores.newInstance(file);
    }

    /**
     * Clears all data from the store, this action is NOT reversible, all data
     * is lost.
     */
    public void clear() {
        store.clear();
    }

    /**
     * After done, the store MUST be closed to flush all changes.
     */
    public void close() {
        store.close();
    }

    public List<String> entityTypes() {
        return store.computeInReadonlyTransaction((txn) -> {
            return txn.getEntityTypes();
        });
    }

    /**
     * Returns an interable of Java beans that match the field and name.
     *
     * @param <T>
     * @param type
     * @param name
     * @param value
     * @return
     * @throws StoreException
     */
    public BeanIterable iter(Class type, String name, @NotNull Comparable value)
            throws StoreException {
        return iter(type, new EqClause(name, value));
    }

    /**
     * Returns an interable with Java beans that matches all clauses.
     *
     * @param <T>
     * @param type
     * @param union
     * @param cl0
     * @param cln
     * @return
     * @throws StoreException
     */
    public BeanIterable iter(Class type, Clause cl0, Clause... cln)
            throws StoreException {

        StoreTransaction tx = store.beginReadonlyTransaction();

        List<Clause> clauses = new ArrayList(Arrays.asList(cln));
        clauses.add(cl0);

        EntityIterable iter = null;

        for (Clause cl : clauses) {

            EntityIterable iter2 = null;
            Oper oper = null;

            if (cl instanceof EqClause) {
                EqClause c = (EqClause) cl;
                iter2 = tx.find(type.getName(), c.getFieldName(), c.getValue());
                oper = c.getOper();
            }
            if (cl instanceof RangeClause) {
                RangeClause c = (RangeClause) cl;
                iter2 = tx.find(type.getName(), c.getFieldName(), c.getFrom(), c.getTo());
                oper = c.getOper();
            }
            if (cl instanceof StartClause) {
                StartClause c = (StartClause) cl;
                iter2 = tx.findStartingWith(type.getName(), c.getFieldName(), c.getStartWith());
                oper = c.getOper();
            }

            if (iter2 == null) {
                throw new StoreException("Unknown clause: " + cl.getClass());
            }

            if (iter == null) {
                iter = iter2;
            } else {
                switch (oper) {
                    case intersect:
                        iter = iter.intersect(iter2);
                        break;
                    case union:
                        iter = iter.union(iter2);
                        break;
                    case concat:
                        iter = iter.concat(iter2);
                        break;
                    case minus:
                        iter = iter.minus(iter2);
                        break;
                    default:
                        throw new StoreException("Unknown clause operation: " + oper);
                }
            }
        }

        return new BeanIterable(type, iter, tx);

    }

    /**
     * Returns an interable of Java beans of the given class.
     *
     * @param <T>
     * @param type
     * @return
     * @throws StoreException
     */
    public BeanIterable iterAll(Class type)
            throws StoreException {
        StoreTransaction tx = store.beginReadonlyTransaction();
        EntityIterable iter = tx.getAll(type.getName());
        return new BeanIterable(type, iter, tx);

    }

    public Object get(Class type, final Long id) {
        return store.computeInTransaction((txn) -> {

            int typeId = store.getEntityTypeId(type.getName());
            if (typeId == -1) {
                return null;
            }

            Object obj = null;
            try {
                Entity e = txn.getEntity(txn.toEntityId(typeId + "-" + id));
                obj = toBean(e, type);
            } catch (EntityRemovedInDatabaseException ignore) {
            }
            return obj;
        });
    }

    /**
     * Insert or updates the given object in the store.
     *
     * @param obj
     * @return
     * @throws StoreException
     */
    public Long store(final Identifiable obj) throws StoreException {

        Object longOrException = store.computeInTransaction((tx) -> {

            String className = obj.getClass().getName();

            Entity e = null;
            Long id = obj.getId();

            if (id != null) {
                try {
                    e = tx.getEntity(tx.toEntityId(store.getEntityTypeId(className) + "-" + id));
                } catch (EntityRemovedInDatabaseException ex) {
                    return new StoreException("No such object; id = " + id, ex);
                }
            }

            if (e == null) {
                e = tx.newEntity(className);
                id = e.getId().getLocalId();
            }

            intoEntity(obj, e);

            return id;
        });

        if (longOrException instanceof StoreException) {
            throw (StoreException) longOrException;
        }
        return (Long) longOrException;
    }

    public Object remove(Identifiable obj) {
        return remove(obj.getClass(), obj.getId());
    }

    public Object remove(Class type, final Long id) {
        return store.computeInTransaction((txn) -> {

            Object obj = null;
            try {
                int typeId = store.getEntityTypeId(type.getName());
                if (typeId == -1) {
                    return null;
                }

                Entity e = txn.getEntity(txn.toEntityId(typeId + "-" + id));
                obj = toBean(e, type);
                if (!e.delete()) {
                    obj = null;
                }
            } catch (EntityRemovedInDatabaseException ignore) {
            }
            return obj;
        });
    }

}
