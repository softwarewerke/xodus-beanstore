/*
Copyright 2016 softwarewerke.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.softwarewerke.xodus.beanstore;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import jetbrains.exodus.entitystore.EntityIterable;
import jetbrains.exodus.entitystore.StoreTransaction;

/**
 * A lazy bean iterable.
 *
 * @author peter
 */
public class BeanIterable<T> implements Iterable<T> {

    EntityIterable entityIterable;
    BeanIterator<T> beanIterator;
    StoreTransaction tx;

    BeanIterable(Class clazz, EntityIterable entityIterable, StoreTransaction tx) {
        this.entityIterable = entityIterable;
        this.beanIterator = new BeanIterator(clazz, entityIterable.iterator());
        this.tx = tx;
    }

    @Override
    public Iterator iterator() {
        return beanIterator;
    }

    /**
     * User code MUST close the iterable when done.
     */
    public void close() {
        tx.abort();
        if (beanIterator.shouldBeDisposed()) {
            beanIterator.dispose();
        }
    }

    /**
     * Return a list of the ids and close.
     *
     * @return
     */
    public List<Long> ids() {
        return ids(true);
    }

    /**
     * Return a list of ids.
     *
     * @param close
     * @return
     */
    public List<Long> ids(boolean close) {
        List<Long> list = new ArrayList();
        while (beanIterator.hasNext()) {
            list.add(((Identifiable) beanIterator.next()).getId());
        }
        if (close) {
            close();
        }
        return list;
    }

    /**
     * Check if is empty and close.
     *
     * @return
     */
    public boolean isEmpty() {
        return isEmpty(true);
    }

    /**
     * Check if empty.
     *
     * @param close
     * @return
     */
    public boolean isEmpty(boolean close) {
        boolean b = entityIterable.isEmpty();
        if (close) {
            close();
        }
        return b;
    }

    /**
     * Get the number of beans retrieved and close.
     *
     * @return
     */
    public long size() {
        return size(true);
    }

    /**
     * Get the number of beans retrieved.
     *
     * @param close
     * @return
     */
    public long size(boolean close) {
        long size = entityIterable.count();
        if (size == -1) {
            size = entityIterable.size();
        }
        if (close) {
            close();
        }
        return size;
    }

    /**
     * Return all matching beans in a List and close.
     *
     * @return
     */
    public List<T> list() {
        return list(true);
    }

    /**
     * Return all matching beans in a List.
     *
     * @param close
     * @return
     */
    public List<T> list(boolean close) {
        List<T> list = new ArrayList();
        while (beanIterator.hasNext()) {
            list.add(beanIterator.next());
        }
        if (close) {
            close();
        }
        return list;
    }

    /**
     * Return one object that matches and close.
     *
     * @return
     */
    public T any() {
        return any(true);
    }

    /**
     * Return first object that matches.
     *
     * @param close
     * @return
     */
    public T any(boolean close) {
        T obj = beanIterator.hasNext() ? beanIterator.next() : null;
        if (close) {
            close();
        }
        return obj;
    }
}
