/*
Copyright 2016 softwarewerke.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.softwarewerke.xodus.beanstore;

import java.util.Objects;

/**
 * A name/value pair used as clause in store querying when searching for value
 * starts with a certain string.
 *
 * @author peter
 */
public class StartClause implements Clause {

    private final Oper oper;
    private final String fieldName;
    private final String startWith;

    public StartClause(Oper oper, String fieldName, String startWith) {
        this.oper = oper;
        this.fieldName = fieldName;
        this.startWith = startWith;
    }

    public StartClause(String fieldName, String startWith) {
        this.oper = Oper.intersect;
        this.fieldName = fieldName;
        this.startWith = startWith;
    }

    public Oper getOper() {
        return oper;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getStartWith() {
        return startWith;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.oper);
        hash = 37 * hash + Objects.hashCode(this.fieldName);
        hash = 37 * hash + Objects.hashCode(this.startWith);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StartClause other = (StartClause) obj;
        if (!Objects.equals(this.fieldName, other.fieldName)) {
            return false;
        }
        if (!Objects.equals(this.startWith, other.startWith)) {
            return false;
        }
        if (this.oper != other.oper) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "StartClause{" + "oper=" + oper + ", fieldName=" + fieldName + ", startWith=" + startWith + '}';
    }


}
