/*
Copyright 2016 softwarewerke.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package test;

import com.softwarewerke.xodus.beanstore.BeanIterable;
import com.softwarewerke.xodus.beanstore.Clause.Oper;
import com.softwarewerke.xodus.beanstore.PersistentBeanStore;
import com.softwarewerke.xodus.beanstore.RangeClause;
import com.softwarewerke.xodus.beanstore.StartClause;
import com.softwarewerke.xodus.beanstore.StoreException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestStoreBench {

    PersistentBeanStore store;

    private static final long WARMUP_ENTRIES = 100;
    private static final int BENCH_THREADS = 20;
    private static final long BENCH_ENTRIES = 100;
    private static final long NAME_POSTFIX = 10000;

    public TestStoreBench() {

    }

    @Before
    public void setUp() {
        store = new PersistentBeanStore("./test-store");
        store.clear();
    }

    @After
    public void tearDown() {
        store.close();
    }

    @Test
    public void benchmark() throws StoreException {

        writer(WARMUP_ENTRIES);
        System.err.println("#entries = " + store.iterAll(Customer.class).list().size());

        store.clear();

        long t = System.currentTimeMillis();

        List<Callable<String>> list = new ArrayList();
        for (int i = BENCH_THREADS; i > 0; i--) {
            list.add(() -> {
                if (Math.random() > 0.8f) {
                    reader();
                    System.err.println("Reader done");
                } else {
                    writer(BENCH_ENTRIES);
                    System.err.println("Writer done");
                }
                return null;
            });
        }
        ExecutorService ex = Executors.newFixedThreadPool(BENCH_THREADS);

        try {
            ex.invokeAll(list);
            ex.shutdown();
            ex.awaitTermination(Long.MAX_VALUE, TimeUnit.HOURS);
        } catch (InterruptedException e) {
        }

        System.err.println("seconds = " + ((System.currentTimeMillis() - t) / 1000f));

        System.err.println("#entries = " + store.iterAll(Customer.class).size());

        System.err.println("customer = " + store.iterAll(Customer.class).any());

        System.err.println("entries = "
                + store.iter(Customer.class, new StartClause("name", "cust-1")).size()
                + " "
                + store.iter(Customer.class, new StartClause("name", "cust-2")).size()
                + " "
                + store.iter(Customer.class, new RangeClause("name", "cust-1", "cust-2Z")).size()
                + " "
                + store.iter(Customer.class,
                        new RangeClause("name", "cust-8", "cust-8Z"),
                        new StartClause(Oper.union, "name", "cust-1")).list().size()
        );

        BeanIterable<Customer> iter = store.iter(Customer.class, new StartClause("name", "cust-12"));
        for (Customer c : iter) {
            System.err.println("Customer = " + c);
        }
        iter.close();

        iter = store.iter(Customer.class, new StartClause("name", "cust-33"));
        System.err.println("Customer = " + iter.list());

        iter = store.iter(Customer.class, new StartClause("name", "cust-4"));
        System.err.println("Customer = " + iter.ids());
    }

    private long writer(long times) throws StoreException {

        long n = 0;
        for (long i = 0; i < times; i++) {
            Customer customer = new Customer();
            customer.setName("cust-" + (int) (Math.random() * NAME_POSTFIX));

            // insert customer with unique name
            synchronized (store) {
                if (store.iter(Customer.class, "name", customer.getName()).any() == null) {
                    store.store(customer);
                    n++;
                }
            }

        }
        return n;
    }

    private void reader() throws StoreException {
        synchronized (store) {
            BeanIterable iter = store.iter(Customer.class, new StartClause("name", "customer-6"));
            iter.close();
        }
    }
}
