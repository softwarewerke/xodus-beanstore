/*
Copyright 2016 softwarewerke.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package test;

import com.softwarewerke.xodus.beanstore.PersistentBeanStore;
import com.softwarewerke.xodus.beanstore.StoreException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestCustomer {

    PersistentBeanStore store;

    public TestCustomer() {
    }

    @Before
    public void setUp() throws StoreException {
        store = new PersistentBeanStore("./test-store");
        store.clear();
        importCountries();
    }

    @After
    public void tearDown() {
        store.close();
    }

    @Test
    public void testUser() throws StoreException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

        Customer customer0 = (Customer) store.get(Customer.class, 0L);
        System.err.println("customer2=" + customer0);

        Customer customer = new Customer();
        customer.setName("joe");
        customer.setFullName("Joe Doe");

        List list = new ArrayList();

        Address addr = new Address();
        addr.setDir("461 Ocean Boulevard");
        addr.setTown("Golden Beach");
        addr.setCountry((Country) store.iter(Country.class, "code", "us").any());
        list.add(addr);

        addr = new Address();
        addr.setDir("3 Savile Row");
        addr.setTown("London");
        addr.setCountry((Country) store.iter(Country.class, "code", "GB").any());
        list.add(addr);

        customer.setAddr(list);

        System.err.println("customer=" + customer);

        long id = 0l;
        try {
            id = store.store(customer);
        } catch (StoreException ex) {
            System.err.println(ex);
        }
        System.err.println("id=" + id);

        Customer customer2 = (Customer) store.get(Customer.class, id);
        System.err.println("customer2=" + customer2);

        customer.setId(10L);
        try {
            id = store.store(customer);
        } catch (StoreException ex) {
            System.err.println(ex);
        }

        List<Customer> listCustomer = (List<Customer>) store.iterAll(Customer.class).list();
        System.err.println("#customer = " + listCustomer.size());

        listCustomer = (List<Customer>) store.iter(Customer.class, "name", "").list();
        System.err.println("#customer = " + listCustomer.size());

        System.err.println(store.entityTypes());
    }

    private void importCountries() throws StoreException {
        Properties p = new Properties();
        try {
            p.load(Country.class.getResourceAsStream("Country.txt"));
        } catch (IOException ex) {
            System.err.println(ex);
        }
        for (Object k : p.keySet()) {
            Country c = new Country();
            c.setName(k.toString());
            c.setCode(p.getProperty(k.toString()));
            store.store(c);
        }

    }
}
